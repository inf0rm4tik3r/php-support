
PHP - Support library

<br>

Installation:

1) Add the following code to your composer.json file.

        "repositories": [
             {
                 "type": "vcs",
                 "url": "https://gitlab.com/inf0rm4tik3r/php-support"
             }
         ],
     
     
2) Add this line to the "require" section in your composer.json file.

        "lukaspotthast/support": "dev-master"