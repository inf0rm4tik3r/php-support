<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Filesystem;

use Lukaspotthast\Support\Exception\Path_Exception;

/**
 * Class Path
 * @package Lukaspotthast\Support\Filesystem
 */
abstract class Path
{

    private const NORMALIZE_INITIAL = 0;

    /**
     * This function only performs a string manipulation! <br>
     * Removes the last $levels parts from $path and returns the remaining string. <br>
     * For example: <br>
     * <pre>
     *      up('/usr/bin') -> '/usr'
     *      up('/usr/bin/') -> '/usr'
     *      up('/usr/bin', 2) -> '/'
     *      up('.') -> '.'
     * </pre>
     *
     * @param string $path
     *      The path to process.
     *
     * @param int    $levels
     *      How many levels to go up. How many parts should be removed from the end of the $path string.
     *
     * @return string
     *      Returns the whole $path up to and including the parent directory.
     */
    public static function up(string $path, int $levels = 1): string
    {
        return dirname($path, $levels);
    }

    /**
     * Expands all symbolic links and resolves references to '/./', '/../' and extra '/' characters in the input path
     * and returns the canonicalized absolute pathname.<br>
     * Path elements must be separated by '/' characters (e.g. 'foo/bar').<br>
     * This function should be used instead of PHPs internal realpath() function if the specified path represents a
     * folder / file which does not jet exists (realpath('/../../foo/bar') would throw an error if foo/bar would not
     * exist two folders up!).
     *
     * @param string
     *      $path The path to process.<br>
     *      May not represent an existing element.<br>
     *      May contain references '/./' and '/../'.
     *
     * @return string
     *      Canonicalized absolute pathname.
     */
    public static function normalize(string $path): string
    {
        return array_reduce(
            explode('/', $path),
            /**
             * @param string|int $carry The return value from the last iteration or the initial value.
             * @param string     $item  The current value to process.
             * @return string           The processed $item.
             */
            function ($carry, string $item): string {
                // Initial call/iteration?
                if ( $carry === self::NORMALIZE_INITIAL )
                {
                    $carry = DIRECTORY_SEPARATOR;
                }

                if ( $item === '' || $item === '.' )
                {
                    return $carry;
                }

                if ( $item === '..' )
                {
                    return dirname($carry, 1);
                }

                return preg_replace("/\/+/", DIRECTORY_SEPARATOR, "$carry/$item");
            },
            self::NORMALIZE_INITIAL
        );
    }

    /**
     * Checks if the specified path is absolute.
     *
     * @param  string $path The path to check.
     * @return bool         true / false
     */
    public static function is_absolute(string $path): bool
    {
        if ( !ctype_print($path) )
        {
            $error_msg = 'Path can NOT have non-printable characters or be empty';
            throw new Path_Exception($error_msg);
        }

        // Optional wrapper(s).
        $regExp = '%^(?<wrappers>(?:[[:print:]]{2,}://)*)';

        // Optional root prefix.
        $regExp .= '(?<root>(?:[[:alpha:]]:/|/)?)';

        // Actual path.
        $regExp .= '(?<path>(?:[[:print:]]*))$%';

        $parts = [];

        if ( !preg_match($regExp, $path, $parts) )
        {
            $error_msg = sprintf('Path "%s" is NOT valid!', $path);
            throw new Path_Exception($error_msg);
        }

        if ( $parts['root'] !== '' )
        {
            return true;
        }

        return false;
    }

    /**
     * Converts all slashes to forward slashes ('/').
     *
     * @param string $path The path to process.
     * @return string      Corrected path.
     */
    public static function convert_slashes(string $path): string
    {
        return str_replace('\\', '/', $path);
    }

    /**
     * Removes leading whitespaces, forward- and back-slashes from the provided path.
     *
     * @param string $path The path to process.
     * @return string      Corrected path.
     */
    public static function remove_leading_slashes(string $path): string
    {
        // Remove general whitespaces.
        $path = ltrim($path);

        // Remove leading forward- and back-slashes.
        $path = ltrim($path, '\/');

        return $path;
    }

}