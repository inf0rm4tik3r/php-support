<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Filesystem;

use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Exception\Directory_Not_Found_Exception;
use Lukaspotthast\Support\Exception\File_Access_Exception;
use Lukaspotthast\Support\Exception\File_Read_Exception;
use Lukaspotthast\Support\Exception\File_Write_Exception;

/**
 * Class Filesystem
 * @package Lukaspotthast\Support\Filesystem
 */
abstract class Filesystem
{

    /**
     * Checks if $filename points to an existing (regular) file. <br>
     * Will return false if $filename points to a directory.
     *
     * @param string $filename
     *      The path to check.
     *
     * @return bool
     *      true / false
     */
    public static function file_exists(string $filename): bool
    {
        return self::is_file($filename);
    }

    /**
     * Checks if $filename represents a regular file.
     *
     * @param string $filename
     *      The path to check.
     *
     * @return bool
     *      true / false
     */
    public static function is_file(string $filename): bool
    {
        return is_file($filename);
    }

    /**
     * Returns the real name of the specified file (excluding the file extension). <br>
     * For example: file_name('.../foo.bar.txt') returns: 'foo.bar'! <br>
     * Make sure that $filename really points to a file by using is_file() beforehand!
     *
     * @param string $filename
     *      The path to check.
     *
     * @return string
     *      The real name of the specified file. See function description.
     */
    public static function file_name(string $filename): string
    {
        return pathinfo($filename, PATHINFO_FILENAME);
    }

    /**
     * Returns the extension of the specified file. <br>
     * Make sure that $filename really points to a file by using is_file() beforehand!
     *
     * @param string $filename
     *      The path to check.
     *
     * @return string
     *      The file extension. For example: 'php'.
     */
    public static function file_extension(string $filename): string
    {
        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    /**
     * Checks if $filename points to an existing directory. <br>
     * Will return false if $filename points to a file.
     *
     * @param string $filename
     *      The path to check.
     *
     * @return bool
     *      true / false
     */
    public static function directory_exists(string $filename): bool
    {
        return self::is_directory($filename);
    }

    /**
     * Checks if $filename represents a folder / directory.
     *
     * @param string $filename
     *      The path to check.
     *
     * @return bool
     *      true / false
     */
    public static function is_directory(string $filename): bool
    {
        return is_dir($filename);
    }

    /**
     * Ensure that the directory specified through $path exists by calling this function. <br>
     * It will create the directory / directories (see $recursive) if they do not already exist.
     *
     * @param string $path
     *      The path to check.
     *
     * @param int $mode
     *      The access modifier for the possibly created folders. <br>
     *      Defaults to: 0777
     *
     * @param bool $recursive
     *      Whether to create a directory for all parts of the path recursively (recommended). <br>
     *      Defaults to: true
     */
    public static function directory_must_exist(string $path, int $mode = 0777, bool $recursive = true): void
    {
        if ( !is_dir($path) ) {
            mkdir($path, $mode, $recursive);
        }
    }

    /**
     * Gets the file size of a given file.
     *
     * @param  string $path The path to check.
     * @return int          The size of the file in bytes.
     */
    public static function size(string $path): int
    {
        $filesize = filesize($path);
        if ( $filesize === false )
        {
            $msg = sprintf('Unable to obtain the size of file "$s"', $path);
            throw new File_Access_Exception($msg);
        }
        return $filesize;
    }

    /**
     * Gets the file's last modification time as a unix timestamp. <br>
     * Ensure that the file exists and is accessible! This function will throw an exception on failure.
     *
     * @param  string $filename
     *      The path to check.
     *
     * @return int
     *      A unix timestamp.
     *
     * @throws File_Access_Exception
     */
    public static function last_modification(string $filename): int
    {
        $filemtime = filemtime($filename);
        if ( $filemtime === false )
        {
            $msg = sprintf('Unable to obtain the last modification time of "$s"', $filename);
            throw new File_Access_Exception($msg);
        }
        return $filemtime;
    }

    /**
     * Gets the file's last access time as a unix timestamp. <br>
     * Ensure that the file exists and is accessible! This function will throw an exception on failure.
     *
     * @param  string $filename
     *      The path to check.
     *
     * @return int
     *      A unix timestamp.
     *
     * @throws File_Access_Exception
     */
    public static function last_access(string $filename): int
    {
        $fileatime = fileatime($filename);
        if ( $fileatime === false )
        {
            $msg = sprintf('Unable to obtain the last access time of "$s"', $filename);
            throw new File_Access_Exception($msg);
        }
        return $fileatime;
    }

    /**
     * Sets both the modification and access time of the specified file to $time.<br>
     * Uses the current system time ( time() ) if no time gets supplied.
     *
     * @param string   $filename The file to modify.
     * @param int|null $time     The new modification and access time to set.
     */
    public static function touch(string $filename, int $time = null): void
    {
        if ( !touch($filename, $time ? $time : time()) )
        {
            $msg = sprintf('Unable to set the modification time of "$s"', $filename);
            throw new File_Access_Exception($msg);
        }
    }

    /**
     * Sets the modification time of the specified file to $time.<br>
     * Uses the current system time ( time() ) if no time gets supplied.
     *
     * @param string   $filename The file to modify.
     * @param int|null $time     The new modification access time to set.
     */
    public static function touch_modification_time(string $filename, int $time = null): void
    {
        $touch = touch(
            // The file to modify.
            $filename,
            // The new modification time to set.
            $time ? $time : time(),
            // Old access time in seconds since the Unix Epoch (January 1 1970 00:00:00 GMT).
            date('U', self::last_access($filename))
        );
        if ( !$touch )
        {
            $msg = sprintf('Unable to set the modification time of "$s"', $filename);
            throw new File_Access_Exception($msg);
        }
    }

    /**
     * Sets the access time of the specified file to $time.<br>
     * Uses the current system time ( time() ) if no time gets supplied.
     *
     * @param string   $filename The file to modify.
     * @param int|null $time     The new access access time to set.
     */
    public static function touch_access_time(string $filename, int $time = null): void
    {
        $touch = touch(
            // The file to modify.
            $filename,
            // Old modification time in seconds since the Unix Epoch (January 1 1970 00:00:00 GMT).
            date('U', self::last_modification($filename)),
            // The new access time to set.
            $time ? $time : time()
        );
        if ( !$touch )
        {
            $msg = sprintf('Unable to set the access time of "$s"', $filename);
            throw new File_Access_Exception($msg);
        }
    }

    /**
     * Reads the specified file. <br>
     * Throws a File_Read_Exception if $filename does not represent a file or if the file can not be read.
     *
     * @param string $filename
     *      The path to the file to read.
     *
     * @return string
     *      The content of the file.
     */
    public static function read(string $filename): string
    {
        if ( !self::file_exists($filename) )
        {
            throw new File_Read_Exception($filename.' does not represent a file!');
        }

        $read = file_get_contents($filename);
        if ( $read === false )
        {
            throw new File_Read_Exception('Could not read file: '.$filename);
        }

        return $read;
    }

    // ... fread

    /**
     * Writes $data $content $filename. <br>
     * Creates directories up to the first parent of $filename and creates the file if necessary. <br>
     * Overwrites the file at $filename if it already exists.
     *
     * @param string $filename
     *      The path to the file which should be written (and created if necessary).
     *
     * @param string $content
     *      The content to write into the file.
     */
    public static function write(string $filename, string $content): void
    {
        self::directory_must_exist(Path::up($filename));

        $written = file_put_contents($filename, $content);
        if ( $written === false )
        {
            throw new File_Write_Exception('Could not write to file: '.$filename);
        }
    }

    /**
     * Writes $content into $filename by opening a file resource with {@link fopen}. <br>
     * Does not check if directories exist up to the first parent of $filename! <br>
     * Creates a file if necessary.
     *
     * @param string $filename
     *      The path (may be an url) to access.
     *
     * @param string $content
     *      The content to write into the destination.
     */
    public static function fwrite(string $filename, string $content): void
    {
        $file = fopen($filename, 'w');
        if ( $file === false )
        {
            throw new File_Write_Exception('Could not open file for writing: '.$filename);
        }

        $written = fwrite($file, $content);
        if ( $written === false )
        {
            throw new File_Write_Exception('Could not write to file: '.$filename);
        }

        $closed = fclose($file);
        if ( $closed === false )
        {
            throw new File_Write_Exception('Could not close file: '.$filename);
        }
    }

    /**
     * Deletes the file specified by $filename. <br>
     * May throw an {@link File_Access_Exception} if the specified $filename does not point to a valid file.
     *
     * @param string $filename
     *      The path of the file to delete.
     */
    public static function delete_file(string $filename): void
    {
        if ( !self::is_file($filename) )
        {
            throw new File_Access_Exception('This is not a file: '.$filename);
        }
        unlink($filename);
    }

    /**
     * Lists all files within the specified $directorypath.
     *
     * @param string $directorypath
     *      The path to read. Make sure that $directorypath really points to a directory by calling is_directory()
     *      beforehand.
     *
     * @param null|array $extensions
     *      OPTIONAL: Array of strings. Only files with the specified file extensions will be included in the result.
     *
     * @return array
     *      Returns the following array:
     *      <pre>
     *          [
     *              'root' => $directorypath,
     *              'files' => [
     *                  0 => [
     *                      'name' => $filename,                 // string
     *                      'file_name' => $file_name,           // string
     *                      'file_extension' => $file_extension, // string
     *                  ],
     *                  // ...
     *              ]
     *          ]
     *      </pre>
     *
     * @throws Directory_Not_Found_Exception
     */
    public static function list_files(string $directorypath, ?array $extensions = null): array
    {
        if ( $extensions !== null )
        {
            Arg::typecheck($extensions, ['string']);
        }

        $list = self::list_elements($directorypath, false);

        $files = [];
        foreach ( $list['elements'] as $key => &$elem_info )
        {
            if ( $elem_info['is_file'] )
            {
                // Limit the result to files of the specified $extensions, if $extensions were set.
                if ( $extensions !== null and !in_array($elem_info['file_extension'], $extensions) )
                {
                    continue;
                }

                array_push($files, [
                    'name' => $elem_info['name'],
                    'file_name' => $elem_info['file_name'],
                    'file_extension' => $elem_info['file_extension']
                ]);
            }
        }

        $list['files'] = $files;
        unset($list['elements']);

        return $list;
    }

    /**
     * Lists all directories within the specified $directorypath.
     *
     * @param string $directorypath
     *      The path to read. Make sure that $directorypath really points to a directory by calling is_directory()
     *      beforehand.
     *
     * @return array
     *      Returns the following array:
     *      <pre>
     *          [
     *              'root' => $directorypath,
     *              'directories' => [
     *                  0 => $directoryname, // string
     *                  // ...
     *              ]
     *          ]
     *      </pre>
     *
     * @throws Directory_Not_Found_Exception
     */
    public static function list_directories(string $directorypath): array
    {
        $list = self::list_elements($directorypath, false);

        $directories = [];
        foreach ( $list['elements'] as $key => &$elem_info )
        {
            if ( $elem_info['is_directory'] )
            {
                array_push($directories, $elem_info['name']);
            }
        }

        $list['directories'] = $directories;
        unset($list['elements']);

        return $list;
    }

    /**
     * Lists all elements (files and directories) within the specified $directorypath.
     *
     * @param string $directorypath
     *      The path to read. Make sure that $directorypath really points to a directory by calling is_directory()
     *      beforehand.
     *
     * @param bool $recursive
     *      This function will recursively load all elements of folders it encounters if $recursive is set to true.
     *      When set to false, only the first level elements will be returned.
     *
     * @return array
     *      Returns the following array:
     *      <pre>
     *          [
     *              'root' => $directorypath,
     *              'elements' => [
     *                  0 => [
     *                      'name' => $filename,                 // string
     *                      'is_file' => $is_file,               // bool
     *                      'file_name' => $file_name,           // string|null
     *                      'file_extension' => $file_extension, // string|null
     *                      'is_directory' => $is_directory,     // bool
     *                      'children' => $children,             // array|null
     *                  ],
     *                  // ...
     *              ]
     *          ]
     *      </pre>
     *
     * @throws Directory_Not_Found_Exception
     */
    public static function list_elements(string $directorypath, bool $recursive = false): array
    {
        if ( !self::is_directory($directorypath) )
        {
            throw new Directory_Not_Found_Exception();
        }

        // The directorypath should always end with a slash.
        if ( mb_substr($directorypath, -1) !== '/' )
        {
            $directorypath = $directorypath.'/';
        }

        $elements = array_diff(
            scandir($directorypath),
            ['.', '..']
        );

        foreach ( $elements as &$name )
        {
            $full_path = $directorypath.$name;

            $is_file = self::is_file($full_path);
            $file_name = $is_file ? self::file_name($full_path) : null;
            $file_extension = $is_file ? self::file_extension($full_path) : null;
            $is_directory = self::is_directory($full_path);
            $children = ($is_directory and $recursive) ?
                self::list_elements($full_path.'/', $recursive) :
                null;

            $name = compact('name', 'is_file', 'file_name', 'file_extension', 'is_directory', 'children');
        }

        $list = [
            'root' => $directorypath,
            'elements' => array_values($elements)
        ];

        return $list;
    }

}