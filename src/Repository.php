<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

/**
 * Class Repository
 * @package Lukaspotthast\Support
 */
class Repository
{

    /**
     * @var array
     */
    private $items;

    /**
     * Repository constructor.
     *
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->init($items);
    }

    /**
     * Resets the repository to its initial state. <br>
     * All contained elements will be lost.
     */
    public function reset(): void
    {
        $this->items = [];
    }

    /**
     * Initializes the repository by either copying or parsing the arrays. <br>
     * Standard behaviour is: copy. Can be changed to parsing by setting $parse = true. <br>
     * See: {@see init_take_array} <br>
     * See: {@see init_parse_array}
     *
     * @param array $items
     * @param bool  $parse
     */
    public function init(array $items, bool $parse = false): void
    {
        if ( !$parse )
        {
            $this->init_take_array($items);
        }
        else
        {
            $this->init_parse_array($items);
        }
    }

    /**
     * Initializes the repository by copying the given array.
     *
     * @param array $items
     */
    public function init_take_array(array $items): void
    {
        $this->items = $items;
    }

    /**
     * Initializes the repository by parsing the specified array. <br>
     * Each key <-> value pair of the array gets passed to the {@link set} function. <br>
     * This function does not iterates over nested arrays.
     *
     * @param array $items
     */
    public function init_parse_array(array $items): void
    {
        foreach ( $items as $path => &$value )
        {
            $this->set($path, $value);
        }
    }

    /**
     * Updates the value stores at $path.
     *
     * @param string $path
     * @param        $value
     */
    public function set(string $path, $value): void
    {
        Arr::set($this->items, $path, $value);
    }

    /**
     * Checks if this repository has a value at $path.
     *
     * @param string $path
     * @return mixed
     */
    public function has(string $path): bool
    {
        return Arr::has($this->items, $path);
    }

    /**
     * Acquires the repository value specified through $path.
     *
     * @param string     $path
     * @param mixed|null $default
     *
     * @return mixed
     *      Either the value found at $path or the specified default value (which itself defaults to: null).
     */
    public function get(string $path, $default = null)
    {
        return Arr::get($this->items, $path, $default);
    }

    /**
     * Returns a copy of the internal array structure. <br>
     *
     * @return array
     */
    public function copy(): array
    {
        return $this->items;
    }

}