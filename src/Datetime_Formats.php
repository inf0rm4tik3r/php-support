<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

/**
 * Trait Datetime_Formats
 * @package Lukaspotthast\Support
 */
trait Datetime_Formats
{

    /** @var string */
    private $date_format_internal;

    /** @var string */
    private $date_separator_internal;

    /** @var string */
    private $time_format_internal;

    /** @var string */
    private $date_format;

    /** @var string */
    private $date_separator;

    /** @var string */
    private $time_format;

    /**
     *
     */
    public function reset_datetime_formats(): void
    {
        $this->date_format_internal    = 'Y-m-d';
        $this->date_separator_internal = ' ';
        $this->time_format_internal    = 'H:i:s';

        $this->date_format    = 'd.m.Y';
        $this->date_separator = ' ';
        $this->time_format    = 'H:i';
    }

    /**
     * @param $time
     * @return int
     */
    private function process_time($time): int
    {
        // If $time got specified as a string, it needs to be processed first.
        if ( is_string($time) )
        {
            if ( $time === 'CURRENT_TIMESTAMP' )
            {
                $time = time();
            }
            else
            {
                $time = strtotime($time);
            }
        }

        // $time should now be an integer. If not, use the current timestamp as a fallback.
        if ( !is_int($time) )
        {
            $time = time();
        }

        return $time;
    }

    /**
     * @param $time
     * @return string
     */
    public function get_datetime_internal($time): string
    {
        // Can not return false, because process_time always returns an int.
        return date(
            $this->date_format_internal . $this->date_separator_internal . $this->time_format_internal,
            $this->process_time($time)
        );
    }

    /**
     * If $time is a string equal to 'CURRENT_TIMESTAMP', the current time will be used.
     * If $time is a string, it gets processed through strtotime().
     * If $time is an integer, it gets used directly.
     *
     * @param $time string|int Can be a string
     * @return string
     */
    public function get_datetime($time): string
    {
        return date(
            $this->date_format . $this->date_separator . $this->time_format,
            $this->process_time($time)
        );
    }

    /**
     * @param $time
     * @return false|string
     */
    public function get_date_internal($time)
    {
        return date($this->date_format_internal, $this->process_time($time));
    }

    /**
     * @param int $time
     * @return string
     */
    public function get_date(int $time): string
    {
        return date($this->date_format, $this->process_time($time));
    }

    /**
     * @param $time
     * @return false|string
     */
    public function get_time_internal($time)
    {
        return date($this->time_format_internal, $this->process_time($time));
    }

    /**
     * @param $time
     * @return false|string
     */
    public function get_time($time)
    {
        return date($this->time_format, $this->process_time($time));
    }

    /* Internal */

    /**
     * @param string $date_format_internal
     */
    public function set_date_format_internal(string $date_format_internal): void
    {
        $this->date_format_internal = $date_format_internal;
    }

    /**
     * @return string
     */
    public function get_date_format_internal(): string
    {
        return $this->date_format_internal;
    }

    /**
     * @param string $date_separator_internal
     */
    public function set_date_separator_internal(string $date_separator_internal): void
    {
        $this->date_separator_internal = $date_separator_internal;
    }

    /**
     * @return string
     */
    public function get_date_separator_internal(): string
    {
        return $this->date_separator_internal;
    }

    /**
     * @param string $time_format_internal
     */
    public function set_time_format_internal(string $time_format_internal): void
    {
        $this->time_format_internal = $time_format_internal;
    }

    /**
     * @return string
     */
    public function get_time_format_internal(): string
    {
        return $this->time_format_internal;
    }

    /* External */

    /**
     * @param string $date_format
     */
    public function set_date_format(string $date_format): void
    {
        $this->date_format = $date_format;
    }

    /**
     * @return string
     */
    public function get_date_format(): string
    {
        return $this->date_format;
    }

    /**
     * @param string $date_separator
     */
    public function set_date_separator(string $date_separator): void
    {
        $this->date_separator = $date_separator;
    }

    /**
     * @return string
     */
    public function get_date_separator(): string
    {
        return $this->date_separator;
    }

    /**
     * @param string $time_format
     */
    public function set_time_format(string $time_format): void
    {
        $this->time_format = $time_format;
    }

    /**
     * @return mixed
     */
    public function get_time_format()
    {
        return $this->time_format;
    }

}