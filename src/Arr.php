<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

use ArrayAccess;
use Lukaspotthast\Support\Exception\Not_Array_Accessible_Exception;
use RuntimeException;

/**
 * Class Arr
 * @package Lukaspotthast\Support
 */
abstract class Arr
{

    const DELIMITER = '.';

    /**
     * Determine whether the given value is accessible like an array.
     *
     * @param mixed $value
     *      The value to check.
     *
     * @return bool
     *      true / false
     */
    public static function is_accessible($value): bool
    {
        return is_array($value) || $value instanceof ArrayAccess;
    }

    /**
     * Determines if the given key exists in the provided array or ArrayAccess object.
     *
     * @param string|int        $key
     *      The key to check.
     *
     * @param ArrayAccess|array $array
     *      The array or ArrayAccess object to check. <br>
     *      Make sure that you only call this function with an argument of type array or ArrayAccess!
     *      The is_accessible() function might help you to determine if that is the case.
     *
     * @return bool
     *      true / false
     */
    public static function key_exists($key, $array): bool
    {
        if ( $array instanceof ArrayAccess )
        {
            return $array->offsetExists($key);
        }

        return array_key_exists($key, $array);
    }

    /**
     * Stores $value at $path in $array if $path does not already exist in $array.
     *
     * @param array|ArrayAccess $array
     *      The array to access. <br>
     *      Must either be an array or an object implementing the ArrayAccess interface.
     *
     * @param string|array|null $path
     *      A path into the array. Each consecutive array access should be denoted by the delimiter sign (default: '.').
     * @param mixed             $value
     *      The value to set at the given $path. This value will replace the $array if $path is null!
     *
     * @param string|null       $delimiter
     *      OPTIONAL: If you want to use a special delimiter string for the array "access parts".
     *      This function will use the Arr::DELIMITER value if this parameter is null.
     *
     * @return array|ArrayAccess
     *      Returns the array untouched if $path already existed or the modified array
     *      (even if the modification happen IN PLACE!).
     */
    public static function add(&$array, $path, $value, ?string $delimiter = null)
    {
        if ( !self::has($array, $path) )
        {
            return self::set($array, $path, $value, $delimiter);
        }

        return $array;
    }

    /**
     * Adds $path to $array and assigns the specified $value to it. This modification is made IN PLACE!
     *
     * @param array|ArrayAccess $array
     *      The array to access. <br>
     *      Must either be an array or an object implementing the ArrayAccess interface.
     *
     * @param string|array|null $path
     *      A path into the array. Each consecutive array access should be denoted by the delimiter sign (default: '.').
     *
     * @param                   $value
     *      The value to set at the given $path. This value will replace the $array if $path is null!
     *
     * @param string|null       $delimiter
     *      OPTIONAL: If you want to use a special delimiter string for the array "access parts".
     *      This function will use the Arr::DELIMITER value if this parameter is null.
     *
     * @return array|ArrayAccess
     *      Returns the modified array (even if the modification happen IN PLACE!).
     *
     * @throws Not_Array_Accessible_Exception
     *      If the specified $array is neither an array nor an instance of ArrayAccess.
     */
    public static function set(&$array, $path, $value, ?string $delimiter = null)
    {
        if ( !self::is_accessible($array) )
        {
            throw new Not_Array_Accessible_Exception();
        }

        if ( is_null($path) )
        {
            return ($array = $value);
        }

        $delimiter = $delimiter !== null ? $delimiter : self::DELIMITER;

        $segments = explode($delimiter, $path);

        while ( count($segments) > 1 )
        {
            $key = array_shift($segments);

            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if ( !isset($array[$key]) || !self::is_accessible($array[$key]) )
            {
                $array[$key] = [];
            }

            $array = &$array[$key];
        }

        $array[array_shift($segments)] = $value;

        return $array;
    }

    /**
     * Checks if a $array is accessible with $path.
     *
     * @param array|ArrayAccess $array
     *      The array to access. <br>
     *      Must either be an array or an object implementing the ArrayAccess interface.
     *
     * @param string|array|null $path
     *      A path into the array. Each consecutive array access should be denoted by the delimiter sign (default: '.').
     * @param null|string       $delimiter
     *      OPTIONAL: If you want to use a special delimiter string for the array "access parts".
     *      This function will use the Arr::DELIMITER value if this parameter is null.
     *
     * @return bool
     *      true / false
     */
    public static function has(&$array, $path, ?string $delimiter = null): bool
    {
        if ( !self::is_accessible($array) )
        {
            throw new Not_Array_Accessible_Exception();
        }

        $delimiter = $delimiter !== null ? $delimiter : self::DELIMITER;

        if ( !is_array($path) )
        {
            if ( is_null($path) or empty($path) )
            {
                return false;
            }

            if ( self::key_exists($path, $array) )
            {
                return true;
            }

            if ( strpos($path, $delimiter) === false )
            {
                return false;
            }

            $segments = explode($delimiter, $path);
        }
        else
        {
            $segments = $path;
        }

        foreach ( $segments as &$segment )
        {
            if ( self::is_accessible($array) && self::key_exists($segment, $array) )
            {
                $array = &$array[$segment];
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Searches through $array and returns the value specified through $path.
     *
     * @param array|ArrayAccess $array
     *      The array to access. <br>
     *      Must either be an array or an object implementing the ArrayAccess interface.
     *
     * @param string|array|null $path
     *      A path into the array. Each consecutive array access should be denoted by the delimiter sign (default: '.').
     *
     * @param mixed             $default
     *      OPTIONAL: This value will be returned if the $path does not exist in $array.
     *
     * @param string|null       $delimiter
     *      OPTIONAL: If you want to use a special delimiter string for the array "access parts".
     *      This function will use the Arr::DELIMITER value if this parameter is null.
     *
     * @return mixed
     *      The value at $path or the $default value if $path could not be found in $array.
     */
    public static function get(&$array, $path, $default = null, ?string $delimiter = null)
    {
        if ( !self::is_accessible($array) )
        {
            return $default;
        }

        $delimiter = $delimiter !== null ? $delimiter : self::DELIMITER;

        if ( !is_array($path) )
        {
            if ( is_null($path) or empty($path) )
            {
                return $array;
            }

            if ( self::key_exists($path, $array) )
            {
                return $array[$path];
            }

            if ( strpos($path, $delimiter) === false )
            {
                return $default;
            }

            $segments = explode($delimiter, $path);
        }
        else
        {
            $segments = $path;
        }

        foreach ( $segments as &$segment )
        {
            if ( self::is_accessible($array) && self::key_exists($segment, $array) )
            {
                $array = &$array[$segment];
            }
            else
            {
                return $default;
            }
        }

        return $array;
    }

    /**
     * Returns an array, which contains only unique elements from the specified array.
     *
     * @param array|ArrayAccess $array
     *      The array to access. <br>
     *      Must either be an array or an object implementing the ArrayAccess interface.
     *
     * @param callable          $eq
     *      Equality-Check-Function for two entries of the array. <br>
     *      This function should return a boolean value
     *
     * @return array|ArrayAccess
     *      Returns the modified array (even if the modification happen IN PLACE!).
     */
    public static function unique(&$array, callable $eq)
    {
        if ( !self::is_accessible($array) )
        {
            throw new Not_Array_Accessible_Exception();
        }

        $unique = [];
        if ( !empty($array) )
        {
            foreach ( $array as &$value )
            {
                $needed = true;
                foreach ( $unique as &$in )
                {
                    $res = call_user_func($eq, $value, $in);
                    if ( !is_bool($res) )
                    {
                        throw new RuntimeException(
                            __CLASS__ . ': ' . __FUNCTION__ . '(): $eq function did not return a bool.'
                        );
                    }
                    // The two entries were equal:
                    if ( $res === true )
                    {
                        // The $value entry is not needed.
                        // It was equal to an entry which we already included.
                        $needed = !$res;
                        break;
                    }
                }
                if ( $needed )
                {
                    array_push($unique, $value);
                }
            }
        }
        return $unique;
    }

}