<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

// TODO: Revise
/**
 * Class Asset_Minifier
 * @package Lukaspotthast\Support
 */
class Asset_Minifier
{

    const JS_MINIFICATION_URL = 'https://javascript-minifier.com/raw';
    const CSS_MINIFICATION_URL = 'https://cssminifier.com/raw';

    /**
     * @param array $files
     */
    public static function minify_js(array $files) : void
    {
        self::minify($files, self::JS_MINIFICATION_URL);
    }

    /**
     * @param $files
     */
    public static function minify_css($files): void
    {
        self::minify($files, self::CSS_MINIFICATION_URL);
    }

    /**
     * @param array  $files
     * @param string $url
     */
    private static function minify(array $files, string $url): void
    {
        foreach ($files as $original => &$destination)
        {
            $handler = fopen($destination, 'w');
            fwrite($handler, self::get_minified($url, file_get_contents($original)));
            fclose($handler);
        }
    }

    /**
     * @param string $url
     * @param string $content
     * @return string
     */
    private static function get_minified(string $url, string $content): string
    {
        $postdata = [
            'http' => [
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query( ['input' => $content] )
            ]
        ];
        return file_get_contents($url, false, stream_context_create($postdata));
    }

}