<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Error;

/**
 * Interface Throws_Errors_Interface
 * @package Lukaspotthast\Support\Error
 */
interface Throws_Errors_Interface
{
    /**
     * @return mixed
     */
    public function reset_errors();

    /**
     * @param string $error_message_prefix
     */
    public function set_error_message_prefix(string $error_message_prefix): void;

    /**
     * @param string     $error_message
     * @param array|null $args
     * @param bool       $only_cache
     */
    public function error(string $error_message, array $args = null, bool $only_cache = false): void;

    /**
     *
     */
    public function publish_cached_errors(): void;

    /**
     * @return mixed
     */
    public function get_errors();

}