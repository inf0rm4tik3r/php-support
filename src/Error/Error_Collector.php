<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Error;

/**
 * Class Error_Collector
 * @package Lukaspotthast\Support\Error
 */
class Error_Collector implements Throws_Errors_Interface
{

    use Throws_Errors_Trait;

    /**
     * Error_Collector constructor.
     */
    public function __construct()
    {
        $this->reset_errors();
    }
}