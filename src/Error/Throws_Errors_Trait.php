<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Error;

use Exception;

/**
 * Trait Throws_Errors
 * @package Lukaspotthast\Crud\Error
 */
trait Throws_Errors_Trait
{

    private $error_bit; // bool
    private $error_messages; // array
    private $error_messages_cache; // array
    private $error_message_prefix; // string

    public function reset_errors() : void
    {
        $this->error_bit = false;
        $this->error_messages = array();
        $this->error_messages_cache = array();
        $this->error_message_prefix = '';
    }

    public function set_error_message_prefix(string $error_message_prefix) : void
    {
        $this->error_message_prefix = $error_message_prefix;
    }

    public function error(string $msg, array $args = null, bool $only_cache = false) : void
    {
        $exception = new Exception;
        $backtrace = $exception->getTraceAsString();

        $error = compact('msg', 'args', 'backtrace');

        // Do not add the error message to this instance directly, if the $only_cache flag is set.
        // Instead push the error to an error cache which can be flushed to the normal error array.
        // This will allow us to generate errors without displaying them in the console / log files without
        // stopping the instance from working.
        if ( $only_cache )
        {
            array_push($this->error_messages_cache, $error);
        }
        // Process the error as normal.
        else
        {
            $this->error_bit = true;
            array_push($this->error_messages, $error);

            $msg = $this->error_message_prefix.$msg;
            //Log::error($msg);
            //Debug::error($msg);
        }
    }

    public function has_errors()
    {
        return $this->error_bit === true;
    }

    public function publish_cached_errors() : void
    {
        foreach ( $this->error_messages_cache as &$error_message )
        {
            array_push($this->error_messages, $error_message);
        }

        $this->error_messages_cache = array();
        $this->error_bit = true;
    }

    public function get_errors()
    {
        return $this->error_messages;
    }

}