<?php

namespace Lukaspotthast\Support\Timing;

use RuntimeException;

/**
 * Class Timer
 * @package Lukaspotthast\Crud\Support\Timing
 */
class Timer
{

    /** @var array */
    private $times;

    /** @var float|null */
    private $last_diff;

    public function __construct()
    {
        $this->times = [];
        $this->last_diff = null;
    }

    public function start(): void
    {
        array_push($this->times, microtime());
    }

    public function has_ongoing_timer(): bool
    {
        return count($this->times) > 0;
    }

    /**
     * @throws RuntimeException If there is currently no timer running.
     */
    public function stop(): float
    {
        if ( !$this->has_ongoing_timer() )
        {
            throw new RuntimeException('There is currently no timer running.');
        }

        $start = array_pop($this->times);
        $end = microtime();

        $this->last_diff = $end;

        return Timing::diff_milli($start, $end);
    }

    /**
     * @return float|null
     */
    public function get_last_diff(): ?float
    {
        return $this->last_diff;
    }

}