<?php

namespace Lukaspotthast\Support\Timing;

class Timing
{

    /**
     * @param string $start_micro String returned from a call to microtime().
     * @param string $end_micro String returned from a call to microtime().
     * @return float Difference between these two times in milliseconds.
     */
    public static function diff(string $start_micro, string $end_micro): float
    {
        return self::diff_milli($start_micro, $end_micro);
    }

    public static function diff_sec(string $start_micro, string $end_micro): float
    {
        $diff_micro_array = self::diff_sec_array($start_micro, $end_micro);
        return $diff_micro_array[0] + $diff_micro_array[1];
    }

    /**
     * @param string $start_micro String returned from a call to microtime().
     * @param string $end_micro String returned from a call to microtime().
     * @return float Difference between these two times in milliseconds.
     */
    public static function diff_milli(string $start_micro, string $end_micro): float
    {
        $diff_micro_array = self::diff_sec_array($start_micro, $end_micro);
        return $diff_micro_array[0] * 1000 + $diff_micro_array[1] * 1000;
    }

    /**
     * @param string $start_micro String returned from a call to microtime().
     * @param string $end_micro String returned from a call to microtime().
     * @return array Difference between these two times in microseconds and a microsecond fraction.
     */
    public static function diff_sec_array(string $start_micro, string $end_micro): array
    {
        $start_parts = self::parse_microtime($start_micro);
        $end_parts = self::parse_microtime($end_micro);

        $microseconds_diff = $end_parts[1] - $start_parts[1];
        $fraction_diff = $end_parts[0] - $start_parts[0];

        if ( $fraction_diff < 0 )
        {
            $microseconds_diff--;
            $fraction_diff = 1 - $fraction_diff;
        }

        return [$microseconds_diff, $fraction_diff];
    }

    public static function parse_microtime(string $microtime): array
    {
        $parts = explode(' ', $microtime);
        $parts[0] = floatval($parts[0]);
        $parts[1] = intval($parts[1]);
        return $parts;
    }

}