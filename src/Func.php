<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

use Closure;
use Lukaspotthast\Support\Error\Throws_Errors_Interface;
use Lukaspotthast\Support\Filesystem\Filesystem;
use Opis\Closure\SerializableClosure;

/**
 * Class Func (Function)
 * @package Lukaspotthast\Crud\Support
 */
class Func
{

    /**
     * Call the specified $function with $arguments. <br>
     * Make sure that the $function argument represents something callable.
     * Use the {@link is_callable()} function to check this.
     *
     * @param       $function
     * @param array $arguments
     * @return mixed
     */
    public static function call($function, array $arguments = []) /* : mixed */
    {
        // Require the file in which the function resides if necessary.
        // Necessary for option 4 found in the is_callable() documentation.
        if ( is_array($function) and is_string($function[0]) and Filesystem::is_file($function[0]) )
        {
            /** @noinspection PhpIncludeInspection */
            require_once($function[0]);
        }

        return call_user_func_array($function, $arguments);
    }

    /**
     * Check if $var is a Closure instance.
     *
     * @param mixed $var
     *      The variable to check.
     *
     * @return bool
     *      true / false
     */
    public static function is_closure($var): bool
    {
        if ( class_exists('SerializableClosure') )
        {
            return is_object($var) and ($var instanceof Closure or $var instanceof SerializableClosure);
        }
        return is_object($var) and $var instanceof Closure;
    }

    /**
     * Check if the $function variable represents something callable. <br>
     * Immediately returns false if an error is found.
     *
     * @param Closure|string|array         $function
     *           Options: <br>
     *           <pre>
     *           1)        callable                                Closure
     *           2)        function_name                           string
     *           3)        [class_name, static_function_name]      [string, string]
     *           4)        [file_path, function_name]              [string, string]
     *           5)        [object, function_name]                 [object, string]
     *           </pre>
     *
     * @param Throws_Errors_Interface|null $err
     *           This function may add errors to the $err object if it got defined.
     *
     * @return bool
     *      true / false
     */
    public static function is_callable($function, ?Throws_Errors_Interface $err): bool
    {
        $function_is_closure = self::is_closure($function);
        $function_is_string  = (is_string($function) and $function !== '');
        $function_is_array =
            is_array($function) and
            (count($function) === 2) and
            array_key_exists(0, $function) and
            array_key_exists(1, $function) and
            (is_object($function[0]) or is_string($function[0])) and
            is_string($function[1]);

        // The function variable needs to be something processable.
        if ( !($function_is_array or $function_is_string or $function_is_closure) )
        {
            $msg = __FUNCTION__ . '(): $function (' . serialize($function) . ') must be a closure, a non empty '
                   . 'string or an array of length 2! A specified array must only contain 2 value, index by 0 and 1, '
                   . 'whereby the first value is either a string or an object and the second value a string.';
            if ( $err !== null )
            {
                $err->error($msg);
            }
            return false;
        }

        // Option 1: Closures are generally callable.
        if ( $function_is_closure /*and is_callable($function)*/ )
        {
            return true;
        }

        // Option 2: Check whether the specified function exists and is callable.
        if ( $function_is_string )
        {
            if ( function_exists($function) and is_callable($function) )
            {
                return true;
            }
            else
            {
                $msg = __FUNCTION__ . '(): The specified function "' . $function . '" does not exist or is not callable.';
                if ( $err !== null )
                {
                    $err->error($msg);
                }
                return false;
            }
        }

        // Options 3-5:Check if the supplied method exists in the specified object and is callable.
        if ( $function_is_array )
        {
            // Options 3+4: [string, string]
            if ( is_string($function[0]) and is_string($function[1]) )
            {
                // Option 3: Statically callable? (Class::function)
                if ( is_callable($function[0] . '::' . $function[1]) )
                {
                    return true;
                }

                // Option 4: Filepath points to a valid file.
                $filename      = $function[0];
                $function_name = $function[1];

                // The filepath must represent a valid file.
                if ( !Filesystem::is_file($filename) )
                {
                    $msg = __FUNCTION__ . '(): The specified file "' . $filename . '" does not exist.';
                    if ( $err !== null )
                    {
                        $err->error($msg);
                    }
                    return false;
                }

                // The file exists. So we can load it up.
                /** @noinspection PhpIncludeInspection */
                require_once($filename);

                // The function must be callable after loading the file.
                if ( function_exists($function_name) and is_callable($function_name) )
                {
                    return true;
                }
                else
                {
                    $msg = __FUNCTION__ . '(): Function "' . $function_name . '" is not callable.';
                    if ( $err !== null )
                    {
                        $err->error($msg);
                    }
                    return false;
                }
            }

            // Option 5: [object, string]
            else
            {
                /** @var object $object */
                $object = $function[0];
                /** @var string $method_name */
                $method_name = $function[1];
                if ( method_exists($object, $method_name) and is_callable($function) )
                {
                    return true;
                }
                else
                {
                    $msg = __FUNCTION__ . '(): The specified method "' . $method_name . '" does not exist or is not callable '
                           . 'in "' . print_r($object) . '".';
                    if ( $err !== null )
                    {
                        $err->error($msg);
                    }
                    return false;
                }
            }
        }

        // Statement should not be reachable. Just for safety reasons.
        return false;
    }

}