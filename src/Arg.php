<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

use Lukaspotthast\Support\Error\Error_Collector;
use Lukaspotthast\Support\Exception\Typecheck_Exception;

/**
 * Class Arg
 * @package Lukaspotthast\Crud\Support
 */
abstract class Arg
{

    /* Available behaviour types. */

    /**
     * Used in: {@link Arg::typecheck} <br>
     * Same as: {@link Arg::THROW} <br><br>
     * Typechecks throw an exception when failing. <br>
     * This is the default behaviour.
     */
    const BEHAVIOUR_THROW = 'throw';
    const THROW           = self::BEHAVIOUR_THROW;

    /**
     * Used in: {@link Arg::typecheck} <br>
     * Same as: {@link Arg::ARRAY} <br><br>
     * Typechecks return an array of failure descriptions.
     */
    const BEHAVIOUR_ARRAY = 'array';
    const ARRAY           = self::BEHAVIOUR_ARRAY;

    /**
     * Used in: {@link Arg::typecheck} <br>
     * Same as: {@link Arg::BOOL} <br><br>
     * Typechecks return a boolean indicating if the check was successful.
     */
    const BEHAVIOUR_BOOL = 'bool';
    const BOOL           = self::BEHAVIOUR_BOOL;

    /**
     * Used in: {@link Arg::typecheck} <br>
     * Same as: {@link Arg::DD} <br><br>
     * Typechecks terminate the current script when failing, while dumping all failures.
     */
    const BEHAVIOUR_DD = 'dd';
    const DD           = self::BEHAVIOUR_DD;

    /**
     * Represents the default behaviour when performing a typecheck.
     * @var string
     */
    public static $typecheck_default_behaviour = self::THROW;

    /**
     * Indicates if typechecks should be performed.
     * @var bool
     */
    public static $typecheck_enabled = true;

    /**
     * Check if $var is of type $type: <br>
     * Calls {@link Arg::typecheck_single} and {@link Arg::typecheck_array} based on the provided input.
     *
     * @param mixed        $var
     *      Must be of type array if $type is an array.
     *
     * @param string|array $type
     *      Type information.
     *
     * @param string       $behaviour
     *      Using {@link Arg::BEHAVIOUR_THROW}, an exception gets thrown if not all typechecks should pass.
     *      If all typechecks pass, true will be returned. <br>
     *      <br>
     *      Use {@link Arg::BEHAVIOUR_ARRAY} to let this function return an array of error messages.
     *      This array will be empty if all typechecks pass. <br>
     *      <br>
     *      Use {@link Arg::BEHAVIOUR_BOOL} to let this function return a boolean specifying if all types matched or
     *      not. <br>
     *      <br>
     *      When using {@link Arg::BEHAVIOUR_DD}, the function will dump occurring error messages and,
     *      if they were any, kill the script.
     *      If all typechecks pass, true will be returned. <br>
     *
     * @return array|bool
     *      The $behaviour description also tells what this function returns.
     */
    public static function typecheck($var, $type, string $behaviour = null) /* mixed */
    {
        // Always and immediately let the typecheck pass if typechecks are globally disabled.
        if ( !self::$typecheck_enabled )
        {
            return true;
        }

        if ( $behaviour === null )
        {
            $behaviour = self::$typecheck_default_behaviour;
        }

        // Perform the typecheck:
        $errors = self::_typecheck($var, $type);

        switch ( $behaviour )
        {
            case self::BEHAVIOUR_THROW:
                if ( !empty($errors) )
                {
                    throw new Typecheck_Exception($errors, 'Types did not match: ' . self::stringify($errors));
                }
                return true;
                break;

            case self::BEHAVIOUR_ARRAY:
                return $errors;
                break;

            case self::BEHAVIOUR_BOOL:
                return empty($errors);
                break;

            case self::BEHAVIOUR_DD:
                if ( !empty($errors) )
                {
                    foreach ( $errors as &$e )
                    {
                        function_exists('dump') ? dump($e) : var_dump($e);
                    }
                    function_exists('dump') ? dump(debug_backtrace()) : var_dump(debug_backtrace());
                    die();
                }
                return true;
                break;

            default:
                die(__CLASS__ . ' - ' . __FUNCTION__ . '(): Unsupported $behaviour: "' . $behaviour . '"');
        }
    }

    /**
     * Check if $var is of type $type: <br>
     * Calls {@link Arg::typecheck_single} and {@link Arg::typecheck_array} based on the provided input.
     *
     * @param mixed        $var
     *      Must be of type array if $type is an array.
     *
     * @param string|array $type
     *      Type information.
     *
     * @return array
     *      An empty array if all types matched, otherwise one error string for each mismatch.
     */
    private static function _typecheck($var, $type): array
    {
        $err = [];

        if ( is_string($type) or (is_array($type) and !is_array($var)) )
        {
            self::_typecheck_single($err, $var, $type);
        }
        else if ( is_array($type) and is_array($var) )
        {
            self::_typecheck_array($err, $var, $type);
        }
        else
        {
            die(__CLASS__ . ' - ' . __FUNCTION__ . '(): Unprocessable input.');
        }

        return $err;
    }

    /**
     * See {@link Arg::typecheck} for an all purpose function. <br>
     * <br>
     * This function is unable to typecheck array keys!! <br>
     * <br>
     * Usage examples:
     * <pre>
     * typecheck_array([4, 'three'],       ['int', 'string'])                  // []
     * typecheck_array([4, 2, 5],          ['int'])                            // [] (the one type 'int' cycles)
     * typecheck_array(['pi', 3.14],       ['float', 'string'])                // ["'pi' is not of type 'float'",
     *                                                                             "'3.14' is not of type 'string'"]
     * typecheck_array([[...], 'three'],   ['array', 'string'])                // []
     * typecheck_array([[5,5,5], 'three'], ['array' => ['int'], 'string'])     // [] (recursive analysis)
     * </pre>
     *
     *
     *
     * @param array $array
     * @param array $types
     * @return array
     */
    public static function typecheck_array(array $array, array $types): array
    {
        // Store all errors found in this array.
        $err = [];

        $type_count = count($types);
        $i          = 0;
        foreach ( $array as &$var )
        {
            // Types must cycle if less types than array entries got specified.
            $index    = $i % $type_count;
            $type     = array_slice($types, $index, 1);
            $type_key = array_keys($types)[$index];

            if ( $type_key === 'array' )
            {
                if ( !is_array($type) )
                {
                    die(__CLASS__ . ': ' . __FUNCTION__ . '(): Array types were not defined as an array!');
                }
                self::_typecheck_single($err, $var, 'array');
                self::_typecheck_array($err, $var, reset($type));
            }
            else
            {
                self::_typecheck_single($err, $var, count($type) === 1 ? reset($type) : $type);
            }

            // Prepare next iteration.
            $i++;
        }

        return $err;
    }

    /**
     * Perform the typechecks and add any errors to the $err array.
     *
     * @param array $err
     * @param array $array
     * @param array $types
     */
    private static function _typecheck_array(array &$err, array $array, array $types): void
    {
        $inner_err = self::typecheck_array($array, $types);
        foreach ( $inner_err as &$msg )
        {
            if ( strlen($msg) !== 0 )
            {
                array_push($err, $msg);
            }
        }
    }

    /**
     * See {@link Arg::typecheck} for an all purpose function. <br>
     * <br>
     * Calculate if $var is of type $type: <br>
     * <br>
     * Usage examples:
     * <pre>
     * typecheck_single(5,    'int')                // ""
     * typecheck_single(5,    'string')             // "5 is not of type 'string'"
     * typecheck_single([..], 'string')             // "[...] is not of type 'string'"
     * typecheck_single([..], 'array')              // ""
     * typecheck_single('foo', [])                  // "'foo' could not be matched with any type of '[...]'"
     * typecheck_single('foo', ['int', 'string'])   // ""
     * </pre>
     *
     * @param mixed        $var
     *      The variable to check. Can be of any type.
     *
     * @param string|array $type
     *      The type or types to check against. Either a simple string, containing one type or an array of strings,
     *      where each string also defines a type.
     *
     * @return string
     *      Return an empty string if the specified type (or one of the specified types (OR)) successfully matched
     *      the type of $var.
     */
    public static function typecheck_single($var, $type): string
    {
        // Match against a single type.
        if ( is_string($type) )
        {
            return self::matches_type($var, $type);
        }
        // Match against a list of types. Pass if one matches.
        else if ( is_array($type) )
        {
            foreach ( $type as &$t )
            {
                $matches_type = self::matches_type($var, $t);
                if ( strlen($matches_type) === 0 )
                {
                    return '';
                }
            }
            return "'" . self::stringify($var) . "' could not be matched with any type of '" . self::stringify($type) . "'!";
        }
        // Unsupported type got supplied.
        else
        {
            die(__FUNCTION__ . '(): $type "' . self::stringify($type) . '" is not a string or an array!');
        }
    }

    /**
     * Perform the typecheck and add any errors to the $err array.
     *
     * @param array        $err
     * @param mixed        $var
     * @param string|array $type
     */
    private static function _typecheck_single(array &$err, $var, $type): void
    {
        $msg = self::typecheck_single($var, $type);
        if ( strlen($msg) !== 0 )
        {
            array_push($err, $msg);
        }
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * Check if $var is of type $type. <br>
     * If $type does not match a predefined type like "!string", its considered a class name. <br>
     * An instanceof check will the be performed to see if $var is an instance of the specified class name. <br>
     * Info: Use {@code Foo::class} to specify the complete class name of Foo (including its namespace)!
     *
     * @param        $var
     *      The variable to check.
     *
     * @param string $type
     *      The type to check against. <br>
     *      One of: <pre>bool, string, char, int, float, double, numeric, array object, callable</pre>
     *      Explanation: <br>
     *      <code>callable</code> gets checked through {@link Func::is_callable}.
     *
     * @return string
     *      Return an empty string if the type of $var matches the specified type or an error message saying
     *      that "'$var' is not of type '$type'".
     */
    private static function matches_type($var, string $type): string
    {
        $info = '';

        switch ( $type )
        {
            case '?':
                // Shortcut: "don't care type"
                $matched = true;
                break;
            case 'isset':
                $matched = isset($var);
                break;
            case 'null':
                $matched = is_null($var);
                break;
            case 'notnull':
                $matched = !is_null($var);
                break;
            case 'bool':
            case 'b':
                $matched = is_bool($var);
                break;
            case '?bool':
                $matched = is_null($var) or is_bool($var);
                break;
            case 'string':
            case 's':
                $matched = is_string($var);
                break;
            case '?string':
                $matched = is_null($var) or is_string($var);
                break;
            case '!string':
                $matched = is_string($var) and !empty($var);
                break;
            case 'char':
            case 'c':
                $matched = is_string($var) and strlen($var) === 1;
                break;
            case '?char':
                $matched = is_null($var) or (is_string($var) and strlen($var) === 1);
                break;
            case 'int':
            case 'i':
                $matched = is_int($var);
                break;
            case '?int':
                $matched = is_null($var) or is_int($var);
                break;
            case 'float':
            case 'f':
                $matched = is_float($var);
                break;
            case '?float':
                $matched = is_null($var) or is_float($var);
                break;
            case 'double':
            case 'd':
                $matched = is_double($var);
                break;
            case '?double':
                $matched = is_null($var) or is_double($var);
                break;
            case 'numeric':
                $matched = is_numeric($var);
                break;
            case '?numeric':
                $matched = is_null($var) or is_numeric($var);
                break;
            case 'alphabetic':
                $matched = ctype_alpha($var);
                break;
            case '?alphabetic':
                $matched = is_null($var) or ctype_alpha($var);
                break;
            case 'alphanumeric':
                $matched = ctype_alnum($var);
                break;
            case '?alphanumeric':
                $matched = is_null($var) or ctype_alnum($var);
                break;
            case 'array':
            case 'a':
                $matched = Arr::is_accessible($var);
                break;
            case '?array':
                $matched = is_null($var) or Arr::is_accessible($var);
                break;
            case '!array':
                $matched = Arr::is_accessible($var) and !empty($var);
                break;
            case 'object':
            case 'o':
                $matched = is_object($var);
                break;
            case '?object':
                $matched = is_null($var) or is_object($var);
                break;
            case 'callable':
                $collector = new Error_Collector();
                $matched   = Func::is_callable($var, $collector);
                $info      = implode('; ', array_map(function ($a) {
                    return $a['msg'];
                }, $collector->get_errors()));
                break;
            case '?callable':
                $collector = new Error_Collector();
                $matched = is_null($var) or Func::is_callable($var, $collector);
                if ( $matched )
                {
                    $info = implode('; ', array_map(function ($a) {
                        return $a['msg'];
                    }, $collector->get_errors()));
                }
                break;
            default:
                /** @noinspection PhpUnhandledExceptionInspection */
                $class   = (new \ReflectionClass($type))->getName();
                $matched = $var instanceof $class;
        }

        return $matched ? '' : "'" . self::stringify($var) . "' is not of type '$type'!" . ' ' . $info;
    }

    /**
     * Get the string representation of the specified element. <br>
     * Return rules: <br>
     * <pre>
     * 1) If $var is an object and implements __toString: $var->__toString() gets returned.
     * 2) If $var is JSON encodable: json_encode($var) gets returned.
     * 2) Otherwise: serialize($var) gets returned.
     * </pre>
     *
     * @param mixed $var
     *      The variable to stringify.
     *
     * @return string
     *      A string representation of the specified variable.
     */
    public static function stringify($var): string
    {
        // A not existing variable cannot be stringified.
        if ( !isset($var) )
        {
            return 'undefined';
        }

        // An object which implements __toString can represent itself.
        if ( is_object($var) and method_exists($var, '__toString') )
        {
            return $var->__toString();
        }

        // Try to encode the $var into JSON:
        $as_json = json_encode($var);
        if ( $as_json !== false )
        {
            return $as_json;
        }

        // $var could not be converted to JSON. It must be serialized.
        return serialize($var);
    }

    /**
     * Toggles the $typecheck_enabled flag of this class.
     */
    public static function toggle_typechecks_enabled(): void
    {
        self::$typecheck_enabled = !self::$typecheck_enabled;
    }

}