<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

/**
 * Class Args
 * @package Lukaspotthast\Support
 */
class Args
{

    /**
     * Generates a basic HTML list from the specified arguments. <br>
     * Each element in the list will state the current argument name and the corresponding value.
     *
     * @param array $arguments
     *      The argument array.
     *
     * @param bool  $include_keys
     *      Whether to print each list item as "key: value" or just "value".
     *
     * @return string
     *      HTML list (<ul>) markup.
     */
    public static function as_list(array $arguments, bool $include_keys = true): string
    {
        $list = '<ul>';
        foreach ( $arguments as $name => &$value )
        {
            if ( !isset($value) )
            {
                $value = 'NULL';
            }
            if ( $include_keys )
            {
                $list .= '<li>$' . $name . ': ' . Arg::stringify($value) . '</li>';
            }
            else
            {
                $list .= '<li>' . Arg::stringify($value) . '</li>';
            }
        }
        $list .= '</ul>';
        return $list;
    }

}