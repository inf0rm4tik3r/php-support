<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

/**
 * Class Color
 * @package Lukaspotthast\Crud\Support
 */
abstract class Color
{

    /**
     * Central luminance value. Colors with a luminance below this value should be considered dark, colors with a
     * luminance above should be considered bright.
     * @var int
     */
    private const LUMINANCE_THRESHOLD = 127;

    /**
     * Check if the specified luminance value is considered DARK.
     *
     * @param float $luminance
     *      The luminance value to check.
     *
     * @return bool
     *      true / false
     */
    public static function is_dark(float $luminance): bool
    {
        return $luminance <= self::LUMINANCE_THRESHOLD;
    }

    /**
     * Check if the specified luminance value is considered BRIGHT.
     *
     * @param float $luminance
     *      The luminance value to check.
     *
     * @return bool
     *      true / false
     */
    public static function is_bright(float $luminance): bool
    {
        return $luminance > self::LUMINANCE_THRESHOLD;
    }

    /**
     * Calculate the luminance value of the given hexadecimal color.
     *
     * @param string $hex_color
     *      Hexadecimal color value. MAY start with a '#' sign.
     *
     * @return float
     *      Luminance value.
     */
    public static function hex_color_luminance(string $hex_color): float
    {
        // Convert from hexadecimal to RGB representation.
        $rgb = self::hex_to_rgb($hex_color);

        // Calculate luminance per "ITU-R BT.709".
        return 0.2126 * $rgb[0] + 0.7152 * $rgb[1] + 0.0722 * $rgb[2];
    }

    /**
     * Convert a hexadecimal color to an HSV color.
     *
     * @param string $hex_color
     *      Hexadecimal color value. MAY start with a '#' sign.
     *
     * @return array
     *      <pre>
     *          [
     *              0 => (float) $hue,
     *              1 => (float) $saturation,
     *              2 => (float) $value
     *          ]
     *      </pre>
     */
    public static function hex_to_hsv(string $hex_color): array
    {
        return self::rgb_to_hsv(self::hex_to_rgb($hex_color));
    }

    /**
     * Convert a hexadecimal color to a YIQ color.
     *
     * @param string $hex_color
     *      Hexadecimal color value. MAY start with a '#' sign.
     *
     * @return array
     *      <pre>
     *          [
     *              0 => (float) $y,
     *              1 => (float) $i,
     *              2 => (float) $q
     *          ]
     *      </pre>
     */
    public static function hex_to_yiq(string $hex_color): array
    {
        return self::rgb_to_yiq(self::hex_to_rgb($hex_color));
    }

    /**
     * Convert a hexadecimal color to an RGB color.
     *
     * @param string $hex_color
     *      Hexadecimal color value. MAY start with a '#' sign.
     *
     * @return array
     *      <pre>
     *          [
     *              0 => (int) $red,
     *              1 => (int) $green,
     *              2 => (int) $blue
     *          ]
     *      </pre>
     *
     * @see http://www.rapidtables.com/convert/color/hex-to-rgb.htm
     */
    public static function hex_to_rgb(string $hex_color): array
    {
        // Strip # if necessary.
        if ( substr($hex_color, 0, 1) === '#' )
        {
            $hex_color = substr($hex_color, 1);
        }

        // Convert rrggbb to decimal.
        $rgb = intval($hex_color, 16);

        return [
            ($rgb >> 16) & 0xFF, // Extract red.
            ($rgb >> 8) & 0xFF, // Extract green.
            ($rgb >> 0) & 0xFF  // Extract blue.
        ];
    }

    /**
     * Convert a RGB color to an HSV color.
     *
     * @param array $rgb_color
     *              <pre>
     *              [
     *              0 => (int) $red,
     *              1 => (int) $green,
     *              2 => (int) $blue
     *              ]
     *              </pre>
     *
     * @return array
     *      <pre>
     *          [
     *              0 => (float) $hue,
     *              1 => (float) $saturation,
     *              2 => (float) $value
     *          ]
     *      </pre>
     *
     * @see http://www.rapidtables.com/convert/color/rgb-to-hsv.htm
     */
    public static function rgb_to_hsv(array $rgb_color): array
    {
        // Normalized rgb values.
        $r = $rgb_color[0] / 255;
        $g = $rgb_color[1] / 255;
        $b = $rgb_color[2] / 255;

        $cmax  = max($r, $g, $b);
        $cmin  = min($r, $g, $b);
        $delta = $cmax - $cmin; // float!

        // Hue calculation:
        $hue = 0;
        if ( $delta == 0 ) // "==" for comparing floats!
        {
            $hue = 0;
        }
        else if ( $cmax === $r )
        {
            $hue = 60 * ((($g - $b) / $delta) % 6);
        }
        else if ( $cmax === $g )
        {
            $hue = 60 * ((($b - $r) / $delta) + 2);
        }
        else if ( $cmax === $b )
        {
            $hue = 60 * ((($r - $g) / $delta) + 4);
        }

        // Saturation calculation:
        $saturation = $cmax === 0 ? 0 : $delta / $cmax;

        // Value calculation:
        $value = $cmax;

        return [$hue, $saturation, $value];
    }

    /**
     * Convert a RGB color to a YIQ color.
     *
     * @param array $rgb_color
     *              <pre>
     *              [
     *              0 => (int) $red,
     *              1 => (int) $green,
     *              2 => (int) $blue
     *              ]
     *              </pre>
     *
     * @return array
     *      <pre>
     *          [
     *              0 => (float) $y,
     *              1 => (float) $i,
     *              2 => (float) $q
     *          ]
     *      </pre>
     *
     * @see https://en.wikipedia.org/wiki/YIQ
     */
    public static function rgb_to_yiq(array $rgb_color): array
    {
        // Normalized rgb values.
        $r = $rgb_color[0] / 255;
        $g = $rgb_color[1] / 255;
        $b = $rgb_color[2] / 255;

        $y = 0.299 * $r + 0.587 * $g + 0.114 * $b;
        $i = 0.596 * $r - 0.274 * $g - 0.322 * $b;
        $q = 0.211 * $r - 0.532 * $g + 0.312 * $b;

        return [$y, $i, $q];
    }

    /**
     * Convert a YIQ color to an RGB color.
     *
     * @param array
     *              <pre>
     *              [
     *              0 => (float) $y,
     *              1 => (float) $i,
     *              2 => (float) $q
     *              ]
     *              </pre>
     *
     * @return array
     *      <pre>
     *          [
     *              0 => (int) $red,
     *              1 => (int) $green,
     *              2 => (int) $blue
     *          ]
     *      </pre>
     *
     * @see https://en.wikipedia.org/wiki/YIQ
     */
    public static function yiq_to_rgb(array $yiq_color): array
    {
        $y = $yiq_color[0];
        $i = $yiq_color[1];
        $q = $yiq_color[2];

        $r = $y + 0.956 * $i + 0.621 * $q;
        $g = $y - 0.272 * $i - 0.647 * $q;
        $b = $y - 1.106 * $i + 1.703 * $q;

        return [$r, $g, $b];
    }

    /**
     * Compare the two specified HSV colors.
     *
     * @param array $a_hsv
     *      The first color in HSV format.
     *
     * @param array $b_hsv
     *      The second color in HSV format.
     *
     * @return int
     *      -1 if $a_hsv comes before $b_hsv, 0 if they are equal, 1 if $b_hsv comes after $a_hsv.
     */
    public static function compare_hsv(array $a_hsv, array $b_hsv): int
    {
        $ah = $a_hsv[0];
        $as = $a_hsv[1];
        $av = $a_hsv[2];

        $bh = $b_hsv[0];
        $bs = $b_hsv[1];
        $bv = $b_hsv[2];

        if ( $ah < $bh )
        {
            return -1;
        }
        else if ( $ah > $bh )
        {
            return 1;
        }

        if ( $as < $bs )
        {
            return -1;
        }
        else if ( $as > $bs )
        {
            return 1;
        }

        if ( $av < $bv )
        {
            return -1;
        }
        else if ( $av > $bv )
        {
            return 1;
        }

        return 0;
    }

    /**
     * Compare the two specified HSV colors.
     *
     * @param array
     *      $a_hsv The first color in HSV format.
     *
     * @param array
     *      $b_hsv The second color in HSV format.
     *
     * @return int
     *      -1 if $a_hsv comes before $b_hsv, 0 if they are equal, 1 if $b_hsv comes after $a_hsv.
     */
    public static function compare_hvs(array $a_hsv, array $b_hsv): int
    {
        $ah = $a_hsv[0];
        $as = $a_hsv[1];
        $av = $a_hsv[2];

        $bh = $b_hsv[0];
        $bs = $b_hsv[1];
        $bv = $b_hsv[2];

        if ( $ah < $bh )
        {
            return -1;
        }
        else if ( $ah > $bh )
        {
            return 1;
        }

        if ( $av < $bv )
        {
            return -1;
        }
        else if ( $av > $bv )
        {
            return 1;
        }

        if ( $as < $bs )
        {
            return -1;
        }
        else if ( $as > $bs )
        {
            return 1;
        }

        return 0;
    }

    /**
     * Compare the two specified HSV colors (first by "Hue" and then by "Value").
     *
     * @param array $a_hsv
     *      The first color in HSV format.
     *
     * @param array $b_hsv
     *      The second color in HSV format.
     *
     * @return int
     *      -1 if $a_hsv comes before $b_hsv, 0 if they are equal, 1 if $b_hsv comes after $a_hsv.
     */
    public static function compare_hv(array $a_hsv, array $b_hsv): int
    {
        $ah = $a_hsv[0];
        $av = $a_hsv[2];

        $bh = $b_hsv[0];
        $bv = $b_hsv[2];

        if ( $ah < $bh )
        {
            return -1;
        }
        else if ( $ah > $bh )
        {
            return 1;
        }

        if ( $av < $bv )
        {
            return -1;
        }
        else if ( $av > $bv )
        {
            return 1;
        }

        return 0;
    }

    /**
     * Compares the two specified YIQ colors.
     *
     * @param array
     *      $a_yiq The first color in YIQ format.
     *
     * @param array
     *      $b_yiq The second color in YIQ format.
     *
     * @return int
     *      -1 if $a_yiq comes before $b_yiq, 0 if they are equal, 1 if $b_yiq comes after $a_yiq.
     */
    public static function compare_yiq(array $a_yiq, array $b_yiq): int
    {
        $ay = $a_yiq[0];
        $ai = $a_yiq[1];
        $aq = $a_yiq[2];

        $by = $b_yiq[0];
        $bi = $b_yiq[1];
        $bq = $b_yiq[2];

        if ( $ay < $by )
        {
            return -1;
        }
        else if ( $ay > $by )
        {
            return 1;
        }

        if ( $ai < $bi )
        {
            return -1;
        }
        else if ( $ai > $bi )
        {
            return 1;
        }

        if ( $aq < $bq )
        {
            return -1;
        }
        else if ( $aq > $bq )
        {
            return 1;
        }

        return 0;
    }

}