<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

use Closure;
use Lukaspotthast\Support\Error\Throws_Errors_Interface;

/**
 * Class Call
 * @package Lukaspotthast\Support
 */
class Call/*able*/
{

    /**
     * @var string|array|Closure
     */
    private $callable;

    /**
     * @var array
     */
    private $arguments;

    /**
     * Call constructor.
     * @param string|array|Closure $callable
     * @param array $arguments
     */
    public function __construct($callable, array $arguments = [])
    {
        $this->set_callable($callable);
        $this->set_arguments($arguments);
    }

    /**
     * @param array|null $arguments
     * @param Throws_Errors_Interface $throws_errors
     * @return mixed
     */
    public function execute(?array $arguments = null, ?Throws_Errors_Interface $throws_errors)
    {
        // Try to call the stored function in the configured file using either the specified arguments or the arguments
        // attached to this callback.
        $result = Func::call($this->callable, isset($arguments) ? $arguments : $this->arguments);

        // The return of false is designated to an error which occurred while calling the function.
        // We have to catch that and set an appropriate error message.
        if ( $result === false )
        {
            $msg = __FUNCTION__.'(): The specified function: "'.Arg::stringify($this->callable).'" produced an error.';
            $throws_errors->error($msg);
        }

        // The result needs to be returned so that it can be further processed by the application.
        return $result;
    }

    /**
     * @param string|array|Closure $callable
     */
    public function set_callable($callable)
    {
        $this->callable = $callable;
    }

    /**
     * @return string|array|Closure
     */
    public function get_callable()
    {
        return $this->callable;
    }

    /**
     * @param array $arguments
     */
    public function set_arguments(array $arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * @return array
     */
    public function get_arguments(): array
    {
        return $this->arguments;
    }

}