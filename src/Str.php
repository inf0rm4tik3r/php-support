<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

/**
 * Class Str
 * @package Lukaspotthast\Support
 */
class Str
{

    const STR_FILL_LEFT  = 1;
    const STR_FILL_RIGHT = 1;

    /**
     * @param string $s
     * @param int    $target_length
     * @return string
     */
    public static function zero_fill_left(string $s, int $target_length): string
    {
        return self::zero_fill($s, $target_length, self::STR_FILL_LEFT);
    }

    /**
     * @param string $s
     * @param int    $target_length
     * @return string
     */
    public static function zero_fill_right(string $s, int $target_length): string
    {
        return self::zero_fill($s, $target_length, self::STR_FILL_RIGHT);
    }

    /**
     * @param string $s
     * @param int    $target_length
     * @param int    $fill_pos
     * @return string
     */
    public static function zero_fill(string $s, int $target_length, int $fill_pos = self::STR_FILL_LEFT): string
    {
        return self::fill($s, '0', $target_length, $fill_pos);
    }

    /**
     * @param string $s
     * @param string $fill
     * @param int    $target_length
     * @param int    $fill_pos
     * @return string
     */
    public static function fill(string $s, string $fill, int $target_length, int $fill_pos = self::STR_FILL_LEFT): string
    {
        while ( ($len = strlen($s)) < $target_length )
        {
            $s = $fill_pos === self::STR_FILL_LEFT ? $fill . $s : $s . $fill;
        }

        return $s;
    }

    /**
     * @param string $string
     * @param int    $amount
     * @return string
     */
    public static function remove_last(string $string, int $amount): string
    {
        return substr($string, 0, $amount * (-1));
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @param int    $offset
     * @return bool
     */
    public static function contains(string $haystack, string $needle, int $offset = 0): bool
    {
        return strpos($haystack, $needle, $offset) !== false;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function starts_with($haystack, $needle)
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function ends_with($haystack, $needle)
    {
        return substr($haystack, -strlen($needle)) === $needle;
    }

    /**
     * @param        $string
     * @param string $separator
     * @return array
     */
    public static function to_array($string, $separator = ',')
    {
        $string_simplified = str_replace(' ', '', $string);
        return explode($separator, $string_simplified);
    }

    /**
     * @param string   $string
     * @param string   $delimiter
     * @param string   $boundary
     * @param int|null $limit
     * @return array
     */
    public static function split_by_boundary(string $string, string $delimiter, string $boundary, ?int $limit = null): array
    {
        $final = [];

        $split       = explode($delimiter, $string);
        $split_count = count($split);

        for ( $i = 0; $i < $split_count; $i++ )
        {
            $part = $split[$i];

            if ( !self::contains($part, $boundary) )
            {
                array_push($final, $part);
            }
            else
            {
                $count = substr_count($part, $boundary);
                $even  = $count % 2 === 0;

                if ( $even )
                {
                    array_push($final, $part);
                }
                else
                {
                    if ( $i < $split_count - 1 )
                    {
                        $split[$i + 1] = $part . $delimiter . $split[$i + 1];
                    }
                    else
                    {
                        //fucked up
                        array_push($final, $part);
                    }
                }
            }
        }

        $final_count = count($final);

        // Collapse parts which exceed the specified limit.
        if ( isset($limit) and $final_count > $limit )
        {
            $arr_copy = $final;
            $final    = [];

            for ( $i = 0; $i < $limit - 1; $i++ )
            {
                $final[$i] = $arr_copy[$i];
                array_shift($arr_copy); // remove first
            }

            $final[$limit - 1] = implode('', $arr_copy); // glue the rest
        }

        // Remove boundary string from parts.
        foreach ( $final as &$part )
        {
            $part = str_replace($boundary, '', $part);
        }

        return $final;
    }

    /**
     * @param string $text
     * @param int    $max_characters
     * @return string
     */
    public static function limit_text(string $text, int $max_characters = 50): string
    {
        // Remove HTML and PHP tags.
        $text = strip_tags($text);

        // Remove
        if ( strlen($text) > $max_characters )
        {
            $text       = mb_substr($text, 0, $max_characters);
            $last_space = strrpos($text, ' ');
            if ( $last_space !== false )
            {
                $text = mb_substr($text, 0, $last_space);
            }
            if ( strlen($text) !== 0 )
            {
                $text .= '&hellip;';
            }
        }
        return $text;
    }

}