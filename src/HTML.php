<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

/**
 * Class HTML
 * @package Lukaspotthast\Crud\Support
 */
abstract class HTML
{

    /**
     * Encodes a possibly incorrect string (considering the HTML specification for id's) to be used as an id attribute.
     *
     * @param  string $id The string which is to be encoded.
     * @return string     The encoded string. Ready to be used as an id attribute.
     */
    public static function encode_id(string $id): string
    {
        return str_replace(
            ['=', '/', '+'],
            ['-', '_', ':'],
            base64_encode($id)
        );
    }

    /**
     * Decodes a string which got previously encoded by encode_id().
     *
     * @param  string $id The encoded string which is to be decoded.
     * @return string     The decoded string.
     */
    public static function decode_id(string $id): string
    {
        return base64_decode(
            str_replace(
                ['-', '_', ':'],
                ['=', '/', '+'],
                $id
            )
        );
    }

}