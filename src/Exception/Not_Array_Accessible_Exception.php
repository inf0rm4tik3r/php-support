<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class Not_Array_Accessible_Exception
 * @package Lukaspotthast\Support\Error\Exceptions
 */
class Not_Array_Accessible_Exception extends _Exception
{

}