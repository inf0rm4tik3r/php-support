<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class Directory_Not_Found_Exception
 * @package Lukaspotthast\Support\Exception
 */
class Directory_Not_Found_Exception extends _Exception
{

}