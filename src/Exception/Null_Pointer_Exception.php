<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class Null_Pointer_Exception
 * @package Lukaspotthast\Support\Exception
 */
class Null_Pointer_Exception extends _Exception
{

}