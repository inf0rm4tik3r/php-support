<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class Array_Index_Not_Found_Exception
 * @package Lukaspotthast\Support\Exceptions
 */
class Array_Index_Not_Found_Exception extends _Exception
{

}