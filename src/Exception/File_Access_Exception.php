<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class File_Access_Exception
 * @package Lukaspotthast\Support\Exception
 */
class File_Access_Exception extends _Exception
{

}