<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

use RuntimeException;

/**
 * Class _Exception
 * @package Lukaspotthast\Support\Exception
 */
class _Exception extends RuntimeException
{

}