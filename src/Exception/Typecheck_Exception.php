<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

use Lukaspotthast\Support\Args;
use Throwable;

/**
 * Class Typecheck_Exception
 * @package Lukaspotthast\Support\Exception
 */
class Typecheck_Exception extends _Exception
{

    /**
     * @var array
     */
    private $type_errors;

    /**
     * Typecheck_Exception constructor.
     * @param array          $type_errors
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(array $type_errors, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->type_errors = $type_errors;
    }

    /**
     * String representation of the exception
     * @link http://php.net/manual/en/exception.tostring.php
     * @return string the string representation of the exception.
     * @since 5.1.0
     */
    public function __toString(): string
    {
        return (parent::__toString().' The following type errors occurred: '.Args::as_list($this->type_errors));
    }

    /**
     * @return array
     */
    public function get_type_errors(): array
    {
        return $this->type_errors;
    }

}