<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class Path_Exception
 * @package Lukaspotthast\Support\Exception
 */
class Path_Exception extends _Exception
{

}