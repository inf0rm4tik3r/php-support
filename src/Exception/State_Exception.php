<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class State_Exception
 * @package Lukaspotthast\Support\Exception
 */
class State_Exception extends _Exception
{

}