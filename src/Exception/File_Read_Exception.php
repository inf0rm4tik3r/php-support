<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class File_Read_Exception
 * @package Lukaspotthast\Support\Exception
 */
class File_Read_Exception extends _Exception
{

}