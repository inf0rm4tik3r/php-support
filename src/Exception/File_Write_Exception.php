<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class File_Write_Exception
 * @package Lukaspotthast\Support\Exception
 */
class File_Write_Exception extends _Exception
{

}