<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support\Exception;

/**
 * Class File_Not_Found_Exception
 * @package Lukaspotthast\Support\Exception
 */
class File_Not_Found_Exception extends _Exception
{

}