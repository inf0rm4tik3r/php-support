<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

use Lukaspotthast\Support\Error\Throws_Errors_Interface;

/**
 * Class Sort
 * @package Lukaspotthast\Support
 */
class Sort
{
    /**
     * Specifies the default sort direction which should be used as a fallback value.
     * @var string
     */
    private const SORT_DIRECTION_DEFAULT = 'asc';

    /**
     * Performs an in-place sorting of the $data array by all specified columns,
     * using either a normal int / string comparison or a user defined function.
     *
     * @param array $data
     *      The array to sort. For example: <br>
     *      <pre>
     *      [
     *          0 => ['id' => 3, 'name' = 'tom'],
     *          1 => ['id' => 1, 'name' = 'eduard'],
     *          2 => ['id' => 2, 'name' = 'peter'],
     *          3 => ['id' => 2, 'name' = 'alfred'],
     *          ...
     *      ]
     *      </pre>
     *      The $data array can be an array of arrays or alternatively an array of objects!
     *
     * @param array $sort_columns
     *      Information on which columns to sort. For example: <br>
     *      <pre>
     *      [
     *          'id' => 'asc',
     *          'name' => 'desc',
     *          ...
     *      ]
     *      </pre>
     *      Make sure that each entry in $data has all keys mentioned in this array. <br>
     *      Make sure that only 'asc' and 'desc' are used as the sort direction.
     *
     * @param array $compare_functions
     *      Specifies if two values of a column should be compared by a function rather then a normal string comparison.
     *      For example: <br>
     *      <pre>
     *      [
     *          'name' => function($direction, $a, $b) {
     *              return $direction === 'asc' ? strcmp($a, $b) : strcmp($b, $a);
     *          },
     *          ...
     *      ]
     *      </pre>
     *      Elements of this array will only be used if the key is also present in $sort_columns. If a function
     *      for comparing colors was specified but it should not be sorted by colors... Well the sorting function
     *      for colors will then never get accessed.
     *
     * @param bool $provide_complete_entries
     *      Specifies if the user defined compare functions get the whole entry instead of just the column value.
     *      Set this flag to true if it is necessary to have access to the whole entries when it comes to comparing
     *      the values of a column. <br>
     *      Default: false
     *
     * @return void Nothing. Modifies the $data array in-place!
     */
    public static function sort_by(
        array &$data,
        array &$sort_columns,
        array &$compare_functions,
        bool $provide_complete_entries = false
    ): void
    {
        usort($data, function($a, $b) use(&$sort_columns, &$compare_functions, $provide_complete_entries): int
        {
            // Lets start with the assumption that the elements are equal.
            $sort_result = 0;

            $a_is_array = is_array($a);
            $b_is_array = is_array($b);

            // Compare values for each specified "sort_by" column.
            foreach ($sort_columns as $column_name => &$direction)
            {
                $a_to_pass = $a_is_array ? $a[$column_name] : $a->{$column_name};
                $b_to_pass = $b_is_array ? $b[$column_name] : $b->{$column_name};

                // User comparison.
                if ( array_key_exists($column_name, $compare_functions) )
                {
                    $func = $compare_functions[$column_name];
                    $sort_result = $provide_complete_entries ?
                        Func::call($func, [$direction, $a, $b]) :
                        Func::call($func, [$direction, $a_to_pass, $b_to_pass]);
                }
                // Int comparison.
                else if ( (is_int($a_to_pass)   and is_int($b_to_pass)  ) or
                          (is_float($a_to_pass) and is_float($b_to_pass)) )
                {
                    $sort_result = $direction === 'asc' ?
                        $a_to_pass - $b_to_pass :
                        $b_to_pass - $a_to_pass;
                }
                // Basic string comparison.
                else
                {
                    $sort_result = $direction === 'asc' ?
                        strcmp((string) $a_to_pass, (string) $b_to_pass) :
                        strcmp((string) $b_to_pass, (string) $a_to_pass);
                }

                // Break out of the loop as soon an inequality of the two compared elements get found.
                if ($sort_result !== 0)
                {
                    break;
                }
            }

            return $sort_result;
        });
    }

    /**
     * Takes any input and returns either 'asc' or 'desc'.
     *
     * @param mixed $direction
     *      Can be an integer of SORT_ASC or SORT_DESC or a string.
     *
     * @param Throws_Errors_Interface $err
     *      Interface for writing errors.
     *
     * @return string
     *      Returns either "asc" or "desc". Returns Sort::SORT_DIRECTION_DEFAULT on failure.
     */
    public static function sanitize_sort_direction($direction, Throws_Errors_Interface $err): string
    {
        if (is_int($direction))
        {
            switch ($direction)
            {
                case SORT_ASC:
                    $direction = 'asc';
                    break;
                case SORT_DESC:
                    $direction = 'desc';
                    break;
                default:
                    $msg = __CLASS__.': '.__FUNCTION__
                        .'(): Sort direction was an integer but neither SORT_ASC nor SORT_DESC!';
                    $err->error($msg);
                    $direction = self::SORT_DIRECTION_DEFAULT;
            }
        }
        else if (is_string($direction))
        {
            $direction = strtolower($direction);

            if ( $direction !== 'asc' and $direction !== 'desc' )
            {
                $err->error(__CLASS__.': '.__FUNCTION__.'(): Sort direction was neither "asc" nor "desc"!');
                $direction = self::SORT_DIRECTION_DEFAULT;
            }
        }
        else
        {
            $err->error(__CLASS__.': '.__FUNCTION__.'(): Sort direction was neither an integer nor a string!');
            $direction = self::SORT_DIRECTION_DEFAULT;
        }
        return $direction;
    }

}