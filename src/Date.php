<?php

declare(strict_types = 1);

namespace Lukaspotthast\Support;

// TODO: revise
/**
 * Class Date
 * @package Lukaspotthast\Support
 */
class Date
{

    /**
     * Converts PHPs date/time format-character to the ones used by the moment.js library.
     */
    private const PHP_TO_MOMENT_FORMAT_REPLACEMENTS = [
        'd' => 'DD',
        'D' => 'ddd',
        'j' => 'D',
        'l' => 'dddd',
        'N' => 'E',
        'S' => 'o',
        'w' => 'e',
        'z' => 'DDD',
        'W' => 'W',
        'F' => 'MMMM',
        'm' => 'MM',
        'M' => 'MMM',
        'n' => 'M',
        't' => '', // no equivalent
        'L' => '', // no equivalent
        'o' => 'YYYY',
        'Y' => 'YYYY',
        'y' => 'YY',
        'a' => 'a',
        'A' => 'A',
        'B' => '', // no equivalent
        'g' => 'h',
        'G' => 'H',
        'h' => 'hh',
        'H' => 'HH',
        'i' => 'mm',
        's' => 'ss',
        'u' => 'SSS',
        'e' => 'zz', // deprecated since version 1.6.0 of moment.js
        'I' => '', // no equivalent
        'O' => '', // no equivalent
        'P' => '', // no equivalent
        'T' => '', // no equivalent
        'Z' => '', // no equivalent
        'c' => '', // no equivalent
        'r' => '', // no equivalent
        'U' => 'X',
    ];

    /**
     * @param string $php_datetime_format
     * @return string
     */
    public static function convert_php_to_moment_format(string $php_datetime_format): string
    {
        return strtr($php_datetime_format, self::PHP_TO_MOMENT_FORMAT_REPLACEMENTS);
    }

    /**
     * @param string $format
     * @param string $date
     * @param string $default
     * @return string
     */
    public static function get_formatted(string $format, string $date, string $default): string
    {
        // Check whether the given value is a correct date.
        if ( strpos($date, '_') !== false )
        {
            // Not a proper date value.
            return date($format, strtotime($default));
        }
        else
        {
            return date($format, strtotime($date));
        }
    }

    private static $days = [
        1 => 'Montag',
        2 => 'Dienstag',
        3 => 'Mittwoch',
        4 => 'Donnerstag',
        5 => 'Freitag',
        6 => 'Samstag',
        7 => 'Sonntag',
    ];

    private static $days_short = [
        1 => 'Mo',
        2 => 'Di',
        3 => 'Mi',
        4 => 'Do',
        5 => 'Fr',
        6 => 'Sa',
        7 => 'So',
    ];

    private static $months = [
        1  => 'Januar',
        2  => 'Februar',
        3  => 'März',
        4  => 'April',
        5  => 'Mai',
        6  => 'Juni',
        7  => 'Juli',
        8  => 'August',
        9  => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Dezember',
    ];

    public static function day_number(int $day, int $month, int $year): string
    {
        return date('N', strtotime($year . '-' . $month . '-' . $day));
    }

    public static function month_in_quarter(int $month, int $quarter): int
    {
        return $month + (($quarter - 1) * 3);
    }

    public static function first_month_in_quarter($quarter)
    {
        return 1 + (($quarter - 1) * 3);
    }

    public static function second_month_in_quarter($quarter)
    {
        return 2 + (($quarter - 1) * 3);
    }

    public static function third_month_in_quarter($quarter)
    {
        return 2 + (($quarter - 1) * 3);
    }

    public static function month_name(int $month): string
    {
        return self::$months[$month];
    }

    public static function is_weekend($day_number)
    {
        return $day_number >= 6;
    }

    public static function day_name(int $day_number)
    {
        return self::$days[$day_number];
    }

    public static function day_name_short(int $day_number)
    {
        return self::$days_short[$day_number];
    }

}